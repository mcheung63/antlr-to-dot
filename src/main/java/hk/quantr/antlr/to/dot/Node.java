package hk.quantr.antlr.to.dot;

import java.util.HashSet;

/**
 *
 * @author peter
 */
public class Node {

	public String name;
	public String body;
	public HashSet<String> children = new HashSet<String>();
	public int level;

	public int hashCode() {
		return 1;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Node)) {
			return false;
		}
		Node n = (Node) o;
		return this.name.equals(n.name);
	}

	@Override
	public String toString() {
		String temp = "";
		temp += name + "\n";
		for (String s : children) {
			temp += "\t" + s + "\n";
		}
		temp += "size=" + children.size() + "\n";
		temp += "------------------------------\n";
		return temp;
	}

//	@Override
//	public int compareTo(Object o) {
//		System.out.println("compareTo");
//		Node n = (Node) o;
//		return this.name.compareTo(n.name);
//	}
}
