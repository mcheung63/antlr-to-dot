package hk.quantr.antlr.to.dot;

import com.peterswing.CommonLib;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.antlr.parser.ANTLRv4Lexer;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class AntlrToDot {

	static HashSet<Node> nodes = new HashSet<Node>();

	public static void main(String args[]) {
		AntlrToDot.generateDotFromFile(new File("/Users/peter/NetBeansProjects/workspace/AntlrTest/src/main/java/com/peter/antlrtest/antlr/Test2.g4"));
	}

	public static void generateDotFromFile(File file) {
		generateDotFromFile(file, null);
	}

	public static void generateDotFromFile(File file, File file2) {
		try {
			ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream(file)));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
			ANTLRv4Parser.GrammarSpecContext context = parser.grammarSpec();
			ParseTreeWalker walker = new ParseTreeWalker();
			MyANTLRv4ParserListener listener = new MyANTLRv4ParserListener(parser, FileUtils.readFileToString(file));
			walker.walk(listener, context);

			if (file2 != null) {
				lexer = new ANTLRv4Lexer(new ANTLRInputStream(new FileInputStream(file2)));
				tokenStream = new CommonTokenStream(lexer);
				parser = new ANTLRv4Parser(tokenStream);
				context = parser.grammarSpec();
				walker = new ParseTreeWalker();
				listener = new MyANTLRv4ParserListener(parser, FileUtils.readFileToString(file2));
				walker.walk(listener, context);
			}

			StringBuilder sb = new StringBuilder();
			sb.append("digraph \"Grammar\" {\n"
					+ "	graph [	"
					+ "		fontname = \"Arial\",\n"
					+ "		splines  = ortho,\n"
					+ "		fontsize = 8,"
					+ "		rankdir=\"LR\",\n"
					+ "	];\n"
					+ "	node [	"
					+ "		shape    = \"record\",\n"
					+ "     style    = \"filled\",\n"
					+ "		fontname = \"Arial\"\n"
					+ "];\n");

			System.out.println(nodes);
			for (Node node : nodes) {
				if (isEmpty(node)) {
					continue;
				}
				String name = filter(node.name);
				sb.append("\"" + Shorten(name) + "\" [label=\"" + Shorten(name) + "\", style=filled, fillcolor=\"" + CommonLib.toHexColor(CommonLib.getLightColor(name, 0.8f)) + "\"]\n");
			}

			String startRule = "assemble";
			HashMap<Integer, ArrayList<String>> nodesInLevel = new HashMap<Integer, ArrayList<String>>();

			sb.append("\n");
			int maxLevel = gen(new HashSet<String>(), startRule, sb, 0, nodesInLevel);
			sb.append("\n");

			System.out.println("maxLevel=" + maxLevel);
			sb.append("}\n");
			System.out.println("nodesInLevel=" + nodesInLevel);

//			sb.append("{\n");
//			for (int x = 0; x <= maxLevel; x++) {
//				sb.append(x + " -> ");
//			}
//			sb.append("\n}\n");
//			for (int level : nodesInLevel.keySet()) {
//				sb.append("{\n");
//				sb.append("rank=same;\"" + level + "\";");
//				for (String n : nodesInLevel.get(level)) {
//					sb.append(n + ";");
//				}
//				sb.append("\n}\n");
//			}
			String dot = sb.toString();
			System.out.println(dot);
			File dotFile = File.createTempFile("export", ".dot");
			FileUtils.writeStringToFile(dotFile, dot, "UTF-8");
			System.out.println("/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o /Users/peter/Desktop/a.png");
			String command = "/opt/local/bin/dot -Tpng " + dotFile.getAbsolutePath() + " -o /Users/peter/Desktop/a.png";
			String result = CommonLib.runCommand(command);
			System.out.println(result);
			dotFile.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static String Shorten(String str) {
		if (str.length() > 10) {
			return str.substring(0, 10) + " ...";
		} else {
			return str;
		}
	}

	private static String filter(String str) {
		return str.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("$", "").replaceAll("\\|", "").replaceAll("\\(", "").replaceAll(";", "").replaceAll("\n", "\\\\l");
	}

	private static int gen(HashSet<String> processed, String rule, StringBuilder sb, int level, HashMap<Integer, ArrayList<String>> nodesInLevel) {
		if (processed.contains(rule)) {
			return level;
		}
		System.out.println("rule=" + rule);
		processed.add(rule);
		for (Node n : nodes) {
			if (!n.name.equals(rule)) {
				continue;
			}
			String name = n.name;
			if (nodesInLevel.get(level) == null) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(name);
				nodesInLevel.put(level, temp);
			} else {
				ArrayList<String> temp = nodesInLevel.get(level);
				temp.add(name);
			}
//			sb.append("\"" + name + "\" [];\n");
			int maxLevel = Integer.MIN_VALUE;
			if (n.name.contains("1'+'NU")) {
				System.out.println("fuck");
			}
			for (String s : n.children) {
				if (isExist(s)) {
					sb.append("\"" + Shorten(name) + "\" -> \"" + Shorten(s) + "\" [width=10, color=\"#000000\", arrowhead=normal];\n");
					int tempLevel = gen(processed, s, sb, level + 1, nodesInLevel);
					if (tempLevel > maxLevel) {
						maxLevel = tempLevel;
					}
				}
			}
			return maxLevel;
		}
		return level;
	}

	private static boolean isExist(String str) {
		for (Node n : nodes) {
			if (n.name.equals(str)) {
				return true;
			}
		}
		return false;
	}

	private static Node getNode(String name) {
		for (Node node : nodes) {
			if (node.name.equals(name)) {
				return node;
			}
		}
		return null;
	}

	private static boolean isEmpty(Node node) {
		if (node == null) {
			return true;
		}
		System.out.println(node.name);
		if (node.children.isEmpty()) {
			return true;
		} else {
			boolean t = false;
			for (String c : node.children) {
				if (c.equals(node.name)) {
					continue;
				}
				boolean d = isEmpty(getNode(c));
				if (d) {
					return true;
				}
			}
			return false;
		}
	}
}
