package hk.quantr.antlr.to.dot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.antlr.parser.ANTLRv4Parser;
import org.antlr.parser.ANTLRv4ParserBaseListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyANTLRv4ParserListener extends ANTLRv4ParserBaseListener {

	public String fullText;
	private final Map<String, Integer> rmap = new HashMap<>();

	Node lastLexerRuleSpec;
	Node lastLexerRuleBlock;

	Node lastParserRuleSpec;
//	Node lastRuleBlock;
	boolean isLexer;

//	String lastBody;
	String red = (char) 27 + "[31m";
	String green = (char) 27 + "[32m";
	String blue = (char) 27 + "[34m";
//	ArrayList<String> temp;

	public MyANTLRv4ParserListener(Parser parser, String fullText) {
		this.fullText = fullText;
		rmap.putAll(parser.getRuleIndexMap());
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		String ruleName = getRuleByKey(ctx.getRuleIndex());
		Token s = ctx.getStart();
		Token e = ctx.getStop();

		System.out.println(">>" + ruleName + "\t\t" + ctx.getText());
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		String ruleName = getRuleByKey(ctx.getRuleIndex());
		Token s = ctx.getStart();
		Token e = ctx.getStop();

		System.out.println("<<" + ruleName + "\t\t" + ctx.getText());
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream()
				.filter(e -> Objects.equals(e.getValue(), key))
				.map(Map.Entry::getKey)
				.findFirst()
				.orElse(null);
	}

	@Override
	public void enterLexerRuleSpec(ANTLRv4Parser.LexerRuleSpecContext ctx) {
		System.out.println(red + "LexerRuleSpec > " + ctx.TOKEN_REF().getText());

		isLexer = true;

		Node node = new Node();
		node.name = ctx.TOKEN_REF().getText();
		AntlrToDot.nodes.add(node);
		lastLexerRuleSpec = node;
	}

	@Override
	public void exitLexerRuleSpec(ANTLRv4Parser.LexerRuleSpecContext ctx) {
		System.out.println(red + "LexerRuleSpec < " + ctx.TOKEN_REF().getText());
	}

	@Override
	public void enterLexerElements(ANTLRv4Parser.LexerElementsContext ctx) {
		System.out.println(red + "\t\tLexerElements > " + ctx.getText());
	}

	@Override
	public void exitLexerElements(ANTLRv4Parser.LexerElementsContext ctx) {
		System.out.println(red + "\t\tLexerElements < " + ctx.getText());
	}

	// rule
	@Override
	public void enterParserRuleSpec(ANTLRv4Parser.ParserRuleSpecContext ctx) {
		System.out.println(red + "ParserRuleSpec > " + ctx.RULE_REF().getText());

		isLexer = false;

		Node node = new Node();
		node.name = ctx.RULE_REF().getText();
		AntlrToDot.nodes.add(node);
		lastParserRuleSpec = node;

	}

	@Override
	public void exitParserRuleSpec(ANTLRv4Parser.ParserRuleSpecContext ctx) {
		System.out.println(red + "ParserRuleSpec < " + ctx.RULE_REF().getText());
	}

	@Override
	public void enterLabeledAlt(ANTLRv4Parser.LabeledAltContext ctx) {
	}

	@Override
	public void enterRuleref(ANTLRv4Parser.RulerefContext ctx) {
		System.out.println(red + "\t\t\t\tRuleref > " + ctx.getText());

		lastParserRuleSpec.children.add(ctx.getText());
	}

	@Override
	public void exitRuleref(ANTLRv4Parser.RulerefContext ctx) {
		System.out.println(red + "\t\t\t\tRuleref < " + ctx.getText());
	}

	@Override
	public void enterTerminal(ANTLRv4Parser.TerminalContext ctx) {
		System.out.println(red + "\t\t\t\tTerminal > " + ctx.getText());

		if (isLexer) {
			lastLexerRuleBlock.children.add(ctx.getText());
		} else {
			lastParserRuleSpec.children.add(ctx.getText());
		}
	}

	@Override
	public void exitTerminal(ANTLRv4Parser.TerminalContext ctx) {
		System.out.println(red + "\t\t\t\tTerminal < " + ctx.getText());
	}

	@Override
	public void enterAlternative(ANTLRv4Parser.AlternativeContext ctx) {
		System.out.println(blue + "\t\t\tAlternative > " + ctx.getText());
	}

	@Override
	public void exitAlternative(ANTLRv4Parser.AlternativeContext ctx) {
		System.out.println(blue + "\t\t\tAlternative < " + ctx.getText());
	}

	@Override
	public void enterRuleBlock(ANTLRv4Parser.RuleBlockContext ctx) {
		System.out.println(green + "\tRule Block > " + ctx.getText());
//
//		lastParserRuleSpec.children.add(ctx.getText());
//
//		Node node = new Node();
//		node.name = ctx.getText();
//		AntlrToDot.nodes.add(node);
//		lastRuleBlock = node;
	}

	@Override
	public void exitRuleBlock(ANTLRv4Parser.RuleBlockContext ctx) {
		System.out.println(green + "\tRule Block < " + ctx.getText());
	}

	@Override
	public void enterLexerRuleBlock(ANTLRv4Parser.LexerRuleBlockContext ctx) {
		System.out.println(green + "\tLexer Rule Block > " + ctx.getText());
		lastLexerRuleSpec.children.add(ctx.getText());

		Node node = new Node();
		node.name = ctx.getText();
		AntlrToDot.nodes.add(node);
		lastLexerRuleBlock = node;
	}

	@Override
	public void exitLexerRuleBlock(ANTLRv4Parser.LexerRuleBlockContext ctx) {
		System.out.println(green + "\tLexer Rule Block < " + ctx.getText());
	}
}
