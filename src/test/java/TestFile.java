
import hk.quantr.antlr.to.dot.AntlrToDot;
import java.io.File;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestFile {

	@Test
	public void test() {
//		AntlrToDot.generateDotFromFile(new File("/Users/peter/NetBeansProjects/workspace/AntlrTest/src/main/java/com/peter/antlrtest/antlr/Test.g4"));
		AntlrToDot.generateDotFromFile(new File("/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4"), new File("/Users/peter/NetBeansProjects/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerParser.g4"));
	}
}
